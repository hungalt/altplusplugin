package com.morcinek.android.codegenerator.plugin.ui;

import com.intellij.openapi.application.ApplicationManager;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.ui.ComboBox;
import com.intellij.openapi.ui.DialogBuilder;
import com.intellij.openapi.ui.DialogWrapper;
import com.intellij.ui.CollectionComboBoxModel;
import com.intellij.ui.JBColor;
import com.intellij.ui.components.JBScrollPane;
import com.intellij.ui.components.JBTextField;
import com.morcinek.android.codegenerator.codegeneration.builders.file.ClassNameBuilder;
import com.morcinek.android.codegenerator.extractor.string.FileNameExtractor;
import com.morcinek.android.codegenerator.plugin.utils.PathHelper;
import org.apache.velocity.util.StringUtils;

import javax.swing.*;
import javax.swing.border.LineBorder;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.io.File;
import java.io.FilenameFilter;
import java.util.*;

/**
 * Copyright 2014 Tomasz Morcinek. All rights reserved.
 */
public class CodeDialogBuilder {

    private final DialogBuilder dialogBuilder;

    private final JPanel topPanel;

    private final JTextArea codeArea;
    private JBTextField packageText;
    private JBTextField sourcePathComboBox;
    private ComboBox packageComboBox;
    private ClassNameChangeListener classNameChangeListener;

    public CodeDialogBuilder(Project project, String title, String producedCode) {
        dialogBuilder = new DialogBuilder(project);
        dialogBuilder.setTitle(title);

        JPanel centerPanel = new JPanel(new BorderLayout());

        codeArea = prepareCodeArea(producedCode);
        centerPanel.add(new JBScrollPane(codeArea), BorderLayout.CENTER);
        dialogBuilder.setCenterPanel(centerPanel);

        topPanel = new JPanel(new GridLayout(0, 2));
        centerPanel.add(topPanel, BorderLayout.PAGE_START);

        dialogBuilder.removeAllActions();
    }

    public void addAction(String title, final Runnable action) {
        addAction(title, action, false);
    }

    public void addAction(String title, final Runnable action, final boolean runWriteAction) {
        dialogBuilder.addAction(new AbstractAction(title) {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (runWriteAction) {
                    ApplicationManager.getApplication().runWriteAction(action);
                } else {
                    action.run();
                }
            }
        });
    }

    public void addPackageSection(String rootPackage, Project project) {
        String projectPath = StringUtils.normalizePath(project.getBasePath());
        String sourcePath = getSourcePath();
        String fullPath = new PathHelper().getFolderPath(sourcePath, rootPackage);
        String[] packages = new File(projectPath + "/" + fullPath).list(new FilenameFilter() {
            @Override
            public boolean accept(File current, String name) {
                return new File(current, name).isDirectory();
            }
        });
        String[] fullPackages;
        if (packages != null) {
            fullPackages = new String[packages.length + 1];
            fullPackages[0] = rootPackage;
            for (int i = 0; i < packages.length; i++) {
                fullPackages[i + 1] = rootPackage + "." + packages[i];
            }
        } else {
            fullPackages = new String[]{rootPackage};
        }
        topPanel.add(new JLabel(StringResources.PACKAGE_LABEL));
        packageComboBox = new ComboBox(new CollectionComboBoxModel(Arrays.asList(fullPackages)));
        packageComboBox.setSelectedItem(fullPackages[0]);
        topPanel.add(packageComboBox);
//        topPanel.add(new JLabel(StringResources.PACKAGE_LABEL));
//        packageText = new JBTextField(defaultText);
//        topPanel.add(packageText);
    }

    public String getPackage() {
        return (String) packageComboBox.getSelectedItem();
    }

    public void addSourcePathSection(java.util.List<String> strings, String defaultValue, String fileName) {
        addMyClassNameSection(strings, fileName);
//        topPanel.add(new JLabel(StringResources.SOURCE_PATH_LABEL));
//        sourcePathComboBox = new ComboBox(new CollectionComboBoxModel(string));
//        sourcePathComboBox.setSelectedItem(defaultValue);
//        topPanel.add(sourcePathComboBox);
    }

    /**
     * Created by hungdt
     */
    public void addMyClassNameSection(java.util.List<String> strings, String fileName) {
        String file_name = new FileNameExtractor().extractFromString(fileName);
        String className = new ClassNameBuilder(file_name).builtString();
        topPanel.add(new JLabel(StringResources.CLASS_NAME));
        sourcePathComboBox = new JBTextField(className);
        sourcePathComboBox.getDocument().addDocumentListener(new DocumentListener() {

            @Override
            public void insertUpdate(DocumentEvent e) {
                updateClassName(sourcePathComboBox.getText());
            }

            @Override
            public void removeUpdate(DocumentEvent e) {
                updateClassName(sourcePathComboBox.getText());
            }

            @Override
            public void changedUpdate(DocumentEvent e) {
                updateClassName(sourcePathComboBox.getText());

            }
        });
        topPanel.add(sourcePathComboBox);
    }

    private void updateClassName(String className) {
        classNameChangeListener.onClassNameChanged(className);
    }

    public void setOnClassNameChangeListener(ClassNameChangeListener classNameChangeListener) {
        this.classNameChangeListener = classNameChangeListener;
    }

    public interface ClassNameChangeListener{
        void onClassNameChanged(String className);
    }
    public String getMyClassName() {
        return sourcePathComboBox.getText();
    }

    public String getSourcePath() {
        return getMySourcePath();
//        return (String) sourcePathComboBox.getSelectedItem();
    }

    /**
     * Created by hungdt
     */
    public String getMySourcePath() {
        return "/app/src/main/java";
    }

    public int showDialog() {
        return dialogBuilder.show();
    }

    public void closeDialog() {
        dialogBuilder.getDialogWrapper().close(DialogWrapper.OK_EXIT_CODE);
    }

    public String getModifiedCode() {
        return codeArea.getText();
    }

    private JTextArea prepareCodeArea(String producedCode) {
        JTextArea codeArea = new JTextArea(producedCode);
        codeArea.setBorder(new LineBorder(JBColor.gray));
        return codeArea;
    }

    public void updateCodeArea(String updatedCode) {
        codeArea.setText(updatedCode);
    }
}
