package com.morcinek.android.codegenerator.plugin.actions;

import com.morcinek.android.codegenerator.codegeneration.providers.ResourceProvidersFactory;
import com.morcinek.android.codegenerator.codegeneration.providers.factories.CustomViewResourceProvidersFactory;

public class ViewAction extends LayoutAction {
    @Override
    protected String getResourceName() {
        return "View";
    }

    @Override
    protected String getTemplateName() {
        return "MvpView_template";
    }

    @Override
    protected ResourceProvidersFactory getResourceProvidersFactory() {
        return new CustomViewResourceProvidersFactory();
    }
}
