package com.morcinek.android.codegenerator.codegeneration.providers.factories;

import com.google.common.collect.Lists;
import com.morcinek.android.codegenerator.codegeneration.providers.ResourceProvider;
import com.morcinek.android.codegenerator.codegeneration.providers.ResourceProvidersFactory;
import com.morcinek.android.codegenerator.codegeneration.providers.resources.DefaultProvider;
import com.morcinek.android.codegenerator.extractor.model.Resource;

/**
 * Created by Nguyen Dang Hung on 8/18/2016.
 */
public class CustomViewResourceProvidersFactory implements ResourceProvidersFactory {
    @Override
    public ResourceProvider createResourceProvider(Resource resource) {
        return new DefaultProvider(resource);
    }

    private boolean isApplicable(Resource resource, String... resourcesNames) {
        return Lists.newArrayList(resourcesNames).contains(resource.getResourceType().getFullName());
    }
}
